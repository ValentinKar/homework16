<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require __DIR__ . '/vendor/autoload.php'; 


$address = 'Тверская 6'; 
$address = !isset($_POST['address']) ? $address : $_POST['address']; 

$api = new \Yandex\Geo\Api();
// Или можно икать по адресу
$api->setQuery( $address );

// Настройка фильтров
$api
    ->setLimit(10) // кол-во результатов
    ->setLang(\Yandex\Geo\Api::LANG_US) // локаль ответа
    ->load();
$response = $api->getResponse();
$response->getFoundCount(); // кол-во найденных адресов
$response->getQuery(); // исходный запрос
$response->getLatitude(); // широта для исходного запроса
$response->getLongitude(); // долгота для исходного запроса
?>
<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Adress</title>

</head>
<body>

<h1>Поиск адреса</h1>

<form method="POST">
    <label for="address">Адрес: </label>
    <input type="text" name="address" id="address" value="<?php echo $address; ?>">
    <button type="submit">Найти координаты</button>
    <br />
</form>

<h2>Найденные координаты</h2>
<ul>
<?php 
// Список найденных точек
$collection = $response->getList();
foreach ($collection as $item) :  ?>
    <li>
    <?php  
    $latitude = $item->getLatitude(); 
    $maps_latitude = isset($maps_latitude) ? $maps_latitude : $latitude; 
    $longitude = $item->getLongitude();  
    $maps_longitude = isset($maps_longitude) ? $maps_longitude : $longitude;        
    ?>
        <a href="address.php?lat=<?php echo $latitude; ?>&long=<?php echo $longitude; ?>">
        широта: 
        <?php  echo $latitude;  ?>, 
        долгота: 
        <?php  echo $longitude;  ?>. 
        </a>
    </li>
<?php endforeach; ?>
</ul>

<div id="map" style="width: 400px; height: 300px"></div>

<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<script type="text/javascript">
  var myMap;
  ymaps.ready(init); // Ожидание загрузки API с сервера Яндекса
  function init () {
    myMap = new ymaps.Map("map", {
      center: [<?php 
        echo $maps_latitude . ', '; 
        echo $maps_longitude; 
      ?>], // Координаты центра карты
      zoom: 10 // Zoom
    });
  }
</script>

</body>
</html>