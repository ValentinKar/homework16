<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require __DIR__ . '/vendor/autoload.php'; 

if ( isset($_GET['lat']) ) { 
$maps_latitude = $_GET['lat']; 
}
else {
$maps_latitude = '55.76'; 
}

if ( isset($_GET['long']) ) { 
$maps_longitude = $_GET['long']; 
}
else {
$maps_longitude = '37.64'; 
}

?>
<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Maps</title>

</head>
<body>

<h1>Адрес на карте</h1>

<div id="map" style="width: 400px; height: 300px"></div>

<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<script type="text/javascript">
  var myMap;
  ymaps.ready(init); // Ожидание загрузки API с сервера Яндекса
  function init () {
    myMap = new ymaps.Map("map", {
      center: [<?php 
        echo $maps_latitude . ', '; 
        echo $maps_longitude; 
      ?>], // Координаты центра карты
      zoom: 10 // Zoom
    });
  }
</script>

</body>
</html>